function MyMongoDB() {
	
	var mongoose = require ("mongoose");
	
	// Here we find an appropriate database to connect to, defaulting to localhost if we don't find one.  
	var uristring = process.env.MONGOLAB_URI || 'mongodb://localhost/ShyTapDB';
	var mongoOptions = { db: { safe: true }};
	
	// Makes connection asynchronously.  Mongoose will queue up database
	// operations and release them when the connection is complete.
	mongoose.connect(uristring, mongoOptions, function (err, res) {
		if (err) { 
			console.log ('ERROR connecting to: ' + uristring + '. ' + err);
		} else {
			console.log ('Succeeded connected to: ' + uristring);
		}
	});
	
	// Schema definition:
	var userSchema = new mongoose.Schema({
		userId : String,   // Facebook user ID
		tkn : String,      // Facebook token
//		g : String,        // User preference for gender
		taps : [ String ]  // The list of FB IDs of the tapped users
	});
	
	// Compiles the schema into a model, opening (or creating, if nonexistent) the 'User' collection in the MongoDB database
	this.User = mongoose.model('User', userSchema);
	
	var that = this;
	
	this.storeAccessToken = function(userId, accessToken, cb) {
		var t = new Date();
		that.getUser(userId, function(user) {
			if (!user) {
				user = new that.User ({
					'userId': userId,
					taps : []
				});
			}
			if (accessToken && accessToken !== user.tkn) {
				user.tkn = accessToken;
				user.save(function (err) {
					printTime("storeAccessToken (saved)", t);
					cb(err, user);
				});
			} else {
				printTime("storeAccessToken (token is up to date)", t);
				cb(null, user);
			}
		});
	};
	
	this.storeTap = function (userId, tappedUserId, cb) {
		var t = new Date();
		that.getUser(userId, function(user) {
			if (user) {
				if (user.taps.indexOf(tappedUserId) == -1) {   // Avoid duplicate taps
					user.taps.push(tappedUserId);
				}
				user.save(function (err) {
					printTime("storeTap", t);
					cb(err, user);
				});
			} else {
				cb("db.storeTap(): User not found:" + userId);
			}
//			else { //TODO: Review this case since now we make sure the user exists at the storeAccessToken.
//				user = new that.User ({     //???
//					'userId': userId,       //???
//					taps : [tappedUserId]   //???
//				});                         //???
//			}                               //???
		});
	};

	this.removeTap = function (userId, tappedUserId, cb) {
		var t = new Date();
		that.getUser(userId, function(user) {
			if (user) {
				var i = user.taps.indexOf(tappedUserId);
				if (i != -1) {
					user.taps.splice(i, 1);
					user.save(function (err) {
						printTime("removeTap", t);
						cb(err, user);
					});
				}
			}
		});
	};

	this.getUser = function (userId, cb) {
		if (!userId) {
			cb(null);
			return;
		}
		var t = new Date();
		that.User.find({ 'userId' : userId}).exec(function(err, result) { // result here is an array of User objects 
			printTime("getUser", t);
			if (err) console.log("Error trying to fetch user's taps: " + err);
			cb((!err && result.length > 0) ? result[0] : null);
		});
	};

	this.findMutualUsers = function (userId, tappedUsers, cb) {
		if (!userId) {
			cb(null);
			return;
		}
		var t = new Date();
		that.User.find({})
			.where('userId').in(tappedUsers)
			.where('taps').in([userId])
			.exec(function(err, result) { 
				printTime("findMutualUsers", t);
				if (err) console.log("Error trying to find mutual taps: " + err);
				cb(err ? null : result);
			});
	};
	
//	this.setGenderPreference = function (userId, accessToken, gender, cb) {
//		var t = new Date();
//		that.User.update({'userId' : userId, 'tkn' : accessToken }, {'g' : gender }, function(err, result) { 
//			printTime("setGenderPreference", t);
//			if (err) console.log("Error: setGenderPreference(): " + err);
//			cb(err, result);
//		});
//		
//	};
}

function printTime(label, startTime) {
	if (process.env.DEBUG) {
		return console.log("+++ " + ((new Date().getTime() - startTime.getTime()) / 1000.0) + " sec. (db." + label + ")");
	}
}

function db() {
	return new MyMongoDB();
}

module.exports.db = db;
