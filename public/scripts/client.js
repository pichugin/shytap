
var SEARCH_FRIENDS_LIMIT = 20;
var SEARCH_PEOPLE_LIMIT = 8;
var FRIENDS_PAGE_LIMIT = 8;


function isFriendsOn() {
	return $("#chkSearchFriends").attr('checked');
}

function searchPeople() {
	if (isFriendsOn()) {
		st_searchFriends();
	} else {
		st_search();
	}
}

function st_search() {
	var s = $.trim($("#searchInput").val()).toLowerCase();
	if (s === '') {
		return;
	}
	var url = '/search?fields=id,name,picture,locale&q=' + encodeURIComponent(s) + 
			'&type=user&limit=' + SEARCH_PEOPLE_LIMIT + '&pretty=0';
			
	$("#searchResult").html(""); // Clean out the old search result
	st_searchByUrl(url, {
		ejsTemplate : 'ejs/searchResult.ejs',
		isTapped : function(user) {
			return myShyTapUser.taps.indexOf(user.id) >= 0;
		},
		resultContainerId : "searchResult",
		moreBtnId : "moreSearchBtn",
		notFoundMsg : "No people found"
	});
	changeHeaderToResults();
}

function st_searchFriends() {
	var s = $.trim($("#searchInput").val()).toLowerCase();
	if (s === '') {
		return;
	}
	var fql_search_friends = "SELECT uid,name,username FROM user WHERE uid IN (SELECT uid2 FROM friend WHERE uid1=me())" +
			" AND strpos(lower(name),'" + s + "')>=0 LIMIT " + SEARCH_FRIENDS_LIMIT;
			
	var url = '/fql?q=' + encodeURIComponent(fql_search_friends);
	$("#searchResult").html(""); // Clean out the old search result
	st_searchByUrl(url, {
		ejsTemplate : 'ejs/friends.ejs',
		isTapped : function(user) {
			return myShyTapUser.taps.indexOf(user.uid + '') >= 0;
		},
		resultContainerId : "searchResult",
		moreBtnId : "moreSearchBtn",
		notFoundMsg : "No friends found"
	});
	changeHeaderToResults();
}

function changeHeaderToResults() {
	$("#clearResults").toggle(true);
	$("#resultsContainer h3").html("Search Results");
}

function clearSearchResults() {
	$('#searchInput').val('');
	loadFriends();
	$("#clearResults").toggle(false);
	$("#resultsContainer h3").html("Friends");
}

function st_searchByUrl(url, options) {
	if (url.indexOf("access_token") < 0) {
		url += getTokenParam();
	}
	FB.api(url, function(res) {
		if (res.error) {
			logToConsole("st_searchByUrl(): " + JSON.stringify(res.error));
		} else {
			// Remove the tapped users from the search result if any:
			var users = res.data;
			if (users) {
				for (i = users.length - 1; i >= 0; i--) {
					if (options.isTapped(users[i])) {
						users.splice(i, 1);
					}
				}
			}
			var html = new EJS({url: options.ejsTemplate}).render(res);
			var ul = $("#" + options.resultContainerId);
			ul.append(html);
			
			var moreBtn = $("#" + options.moreBtnId).unbind("click").hide();
			var nextUrl = res.paging ? res.paging.next : null; 
			if (nextUrl) {
				moreBtn.click(function() { st_searchByUrl(nextUrl, options); }).show();
			} 
			if (ul.children("li").length === 0) {
				ul.html("<li>" + options.notFoundMsg + "</li>");
			}
		}
	});
}

function st_tap(userId, obj) {
	obj = $(obj);
	var isOff = obj.hasClass('heart-off');

	if (isOff) {
		var userLi = obj.parent();
		jConfirm("Confirm", 
				"Are you sure you want to tap " + userLi.find(".name-container a").text() + "? <small>Remember, your tap stays completely confidential unless the user you tap taps you back.</small>", 
				function(){ st_tap_post(userId, obj, isOff) });
	} else {
		st_tap_post(userId, obj, isOff);
	}
}

function st_tap_post(userId, obj, isOff) {
	var params = { 
			"accessToken" : getToken(),
			"userId" : userId, 
			"myUid" : myShyTapUser.userId 
		};
	
	$.post(isOff ? "tap" : "untap", params, function (data) {
		data = JSON.parse(data);
		if (!data.error) {
			myMutualTaps = data;
			if (isOff) {
				myShyTapUser.taps.unshift(userId); // Add to the beginning of the taps array.
				obj.toggleClass("heart-off heart-on");
				makeTapFly(obj.parent(), isOff, function() {
					obj.parent().prependTo($("#tapsUL"));
				});
				if (myMutualTaps.indexOf(userId) >= 0) {
					obj.addClass("mutual");
					loadMutualNames();
				}
			} else {
				myShyTapUser.taps.splice(myShyTapUser.taps.indexOf(userId), 1);
				if (isFriendsOn()) {
					FB.api('/me/friends/' + userId + "?" + getTokenParam(), function(res) {
						if (res.data.length > 0) {
							obj.toggleClass("heart-off heart-on");
							makeTapFly(obj.parent(), isOff, function() {
								obj.parent().prependTo($("#searchResult"));
							});
							obj.removeClass("mutual"); //If any.
						} else {
							makeTapFly(obj.parent(), isOff, function() {
								obj.parent().remove();
							});
						}
						loadMutualNames();
					});
				} else {
					makeTapFly(obj.parent(), isOff, function() {
						obj.parent().remove();
					});
				}
			}
		} else {
			logToConsole("st_tap(): " + JSON.stringify(data.error));
		}
	});
}

/*
 * @param li The jQuery object that corresponds to the <li> element holding the tap
 */
function makeTapFly(li, isOff, cb) {
	var initPos = li.position(); //li.offset();
	var initWidth = li.width();
	var destPos = isOff ? 
			$("#tapsContainer").offset() :
			$("#searchResult").offset();

	var distanceX = '+=' + (destPos.left - li.offset().left) + 'px';
	var distanceY = '+=' + (destPos.top - li.offset().top) + 'px';
	
	li.css({"position": "absolute",
		"left": initPos.left,
		"top": initPos.top,
		"width": initWidth
	});
	
	/*
	 * Duration
	 * - desktop (width=1000): 700
	 * - phone (width=640): 1400
	 */
	var duration = (1249 - $(window).width()) * 2.3;
	duration = (duration < 600) ? 600 : duration;
	
	li.animate({
			left: distanceX,
			opacity: '0',
			top: distanceY
	}, {
		duration: duration,
		complete: function() {
			$(this).css({"left": "",
					"opacity": "",
					"position": "",
					"top": "",
					"width": ""
				});
			cb();
		}
	});	
}

function loadTaps() {
	if (!myShyTapUser.taps || myShyTapUser.taps.length === 0) {
			var html = new EJS({url: 'ejs/taps.ejs'}).render({ tappedUsers : [] });
			$("#tapsContainer").html(html);
		return;
	}
	// Get tapped users' info:
	FB.api('/?ids=' + myShyTapUser.taps.join(",") + '&fields=name' + getTokenParam(), function(tappedUsers) {
		if (tappedUsers.error) {
			logToConsole("loadTaps(): " + JSON.stringify(tappedUsers.error));
		} else {
			var mutualNames = [];
			var tappedUsersArray = [];
			for (var i in tappedUsers) {
				var user = { id : i, name : tappedUsers[i].name };
				tappedUsersArray.push(user);
				if (myMutualTaps.indexOf(i + "") >= 0) {
					user.mutual = true;
					mutualNames.push(user.name);
				}
			}
			sortTaps(tappedUsersArray, myMutualTaps);
			showMutualNotification(mutualNames.join(", "));
			var html = new EJS({url: 'ejs/taps.ejs'}).render({ tappedUsers : tappedUsersArray });
			$("#tapsContainer").html(html);
		}
	});
}

function sortTaps(tappedUsers, mutualTaps) {
	if (tappedUsers.length === 0 || mutualTaps.length === 0) {
		return;
	}
	tappedUsers.sort(function(a, b) {
		var aa = a.name.toUpperCase();
		var bb = b.name.toUpperCase();
		return aa > bb ? 1 : (aa < bb ? -1 : 0); // ? 0 : 1;
	});
	tappedUsers.sort(function(a, b) {
		var aMutual = mutualTaps.indexOf(a.id) > -1;
		var bMutual = mutualTaps.indexOf(b.id) > -1;
		return !aMutual && bMutual ? 1 : (aMutual && !bMutual ? -1 : 0); //a.name < b.name ? 0 : 1;
	});
}

function showMutualNotification(names) {
	if (names.length > 0) {
		$("#messagePane").html("Congrats! You have been tapped back by: " + names).show();
	} else {
		$("#messagePane").hide();
	}
}

function loadMutualNames() {
	if (myMutualTaps.length === 0) {
		showMutualNotification('');
		return;
	}
	FB.api('/?ids=' + myMutualTaps.join(",") + '&fields=name' + getTokenParam(), function(users) {
		if (users.error) {
			logToConsole("loadMutualNames(): " + JSON.stringify(users.error));
		} else {
			var names = [];
			for (var i in users) {
				names.push(users[i].name);
			}
			showMutualNotification(names.join(", "));
		}
	});
}

function loadFriends(lastName) {
	var fql = 'SELECT uid, name, is_app_user FROM user WHERE uid in (SELECT uid2 FROM friend WHERE uid1 = me())';
	if (myShyTapUser.taps && myShyTapUser.taps.length > 0) {
		fql += ' AND NOT (uid IN (' + myShyTapUser.taps.join(",") + "))";
	}
	if (lastName && lastName !== '') {
		fql += " AND name > '" + lastName + "'";
	}
	fql += ' ORDER BY name LIMIT ' + FRIENDS_PAGE_LIMIT;
	FB.api('/fql?q=' + encodeURIComponent(fql) + getTokenParam(), function(res) {
		var moreBtn = $("#moreSearchBtn").unbind("click").hide();
		if (res.error) {
			logToConsole("FQL request failed. FQL:" + fql + ". Error:" + JSON.stringify(res.error));
		} else if (res.data) {
			var html = new EJS({url: 'ejs/friends.ejs'}).render(res);
			if (res.data.length > 0) {
				var list = $(html);
				if (res.data.length >= FRIENDS_PAGE_LIMIT) {
					moreBtn.click(function() { loadFriends(list.last().find("span.name-container a").text()); }).show();
				}
			}
			if (lastName) {
				$("#searchResult").append(html);
			} else {
				$("#searchResult").html(html);
			}
		}
	});
}

function showMyName() {
	FB.api('/me?' + getTokenParam(), function(res) {
		$("#myName").html(res.name);
	});
}

/* function loginPopup() {	FB.login(loginHandler); } */

function loginHandler(response) {
	if (response.authResponse && response.status === 'connected') {
		$("#accessToken").val(response.authResponse.accessToken);
		var params = { 
			"accessToken" : response.authResponse.accessToken, 
			"uid" : response.authResponse.userID 
		};
		$.get("user", params, function (data) {
			data = JSON.parse(data);
			myMutualTaps = data.mutualTaps || [];
			myShyTapUser = data.myShyTapUser || {userId : response.authResponse.userID};
			loadData();
		});
	} else { 
		logToConsole('User cancelled login or did not fully authorize.');
	}
}

function logToConsole(s) { 
	if (s && (typeof console !== "undefined") && (typeof console.log === "function")) { 
		try {
			console.log(s); 
		} catch (e) { /* ignore */ }
	} 
}

function scrollToAnchor(name){
	var aTag = $("a[name='"+ name +"']");
	$('html,body').animate({scrollTop: aTag.offset().top},'slow');
}

function loadData() {
	$("#headerContainer").html(new EJS({ url : "ejs/userHeader.ejs"}).render({user : {id : myShyTapUser.userId}}));
	$("#mainBody").html($(new EJS({ url : "ejs/mainContainer.ejs"}).render({})));

	//Now we need to manually parse the FB Like button after it was rendered via EJS:
	try {
		FB.XFBML.parse($("#share-app li fb:like").get(0));
	} catch (e) { 
		console.log(e) 
	}

	$('#sendRequest').click(function() { 
		FB.ui({ method : 'apprequests', message : $(this).attr('data-message') }, function (response) { logToConsole(response); });
	});

	showMyName();
	loadTaps();
	loadFriends();
}

function jConfirm(title, msg, cbOk) {
	var windowWidth = $(window).width();
	var popupWidth = Math.min(windowWidth - 20, 390);
	
	$("#jDialog").html(msg);
	$("#jDialog").dialog({
	      autoOpen: true,
	      height: 300,
	      width: popupWidth,
	      modal: true,
	      title: title,
	      buttons: {
	    	  "OK" : function() { $("#jDialog").dialog("close"); cbOk(); },
	    	  "Cancel" : function() { $("#jDialog").dialog("close"); }
	      }
	});
}

function runTutorialInBody() {
	$("#tutorialContainer").html(new EJS({ url : "ejs/tutorial.ejs"}).render());
	$('.flexslider').flexslider({slideshowSpeed: 7000, pauseOnHover: true});
	$("#tutorialContainer").show();
}

function runTutorialInDialog() {
	var windowWidth = $(window).width();
	var popupWidth = Math.min(windowWidth - 20, 600);
	
	$("#jDialog").html(new EJS({ url : "ejs/tutorial.ejs"}).render());
	$('.flexslider').flexslider({slideshowSpeed: 7000, pauseOnHover: true});
	$("#jDialog").dialog({
	      autoOpen: true,
	      height: 600,
	      width: popupWidth,
	      modal: true,
	      title: "Getting Started",
	      buttons: {}
	});
}

function getToken() {
	return FB.getAuthResponse()['accessToken'];
}

function getTokenParam() {
	return "&access_token=" + getToken();
}

$(document).ready(function() {
	$("#jDialog").dialog({autoOpen: false});
});

