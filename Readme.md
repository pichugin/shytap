ShyTap -- Show romantic interest by "tapping" people.
=====================================================

This is an implementation of the [ShyTap](http://shytap.com/) idea. It is based on [Node.js](http://nodejs.org/), [MongoDB](http://www.mongodb.org/) and [Facebook Graph API](http://developers.facebook.com/docs/reference/api/). You can play with it [here](http://shytap.herokuapp.com/) and it is also available in the [Facebook App Center](http://www.facebook.com/appcenter/shy-tap?fb_source=appcenter). The application requires a Facebook account to be able to login and use the functionality.

Run locally
-----------

Install dependencies:

    npm bundle install

[Create an app on Facebook](https://developers.facebook.com/apps) and set the Website URL to `http://localhost:3000/`.

Export the App ID and Secret from the Facebook app settings page as environment variables before starting the app:

    export FACEBOOK_APP_ID=12345
    export FACEBOOK_SECRET=abcde
    export DEBUG=true

The DEBUG variable is optional. It enables additional logging messages on the server.

Launch the app:

    node web.js
    
Either open the direct URL `http://localhost:3000/` or go to the app through the Facebook canvas: `http://apps.facebook.com/<your-app-name>/`

