//var async   = require('async');
var express = require('express');
var util    = require('util');
var restler = require('restler');

var db = require('./db.js').db();

var FB_GRAPH = "https://graph.facebook.com";
var APP_ID = process.env.FACEBOOK_APP_ID;
var SECRET = process.env.FACEBOOK_SECRET;
var DEBUG  = process.env.DEBUG;

var app = express.createServer(
		express.logger(), 
		express.static(__dirname + '/public', { maxAge : 86400000 /* 24 hours */ }), 
		express.bodyParser(), 
		express.cookieParser(),
		// set this to a secret value to encrypt session cookies
//		express.session({ secret : process.env.SESSION_SECRET || 'secret123' }),
		function (req, res, next) {
			var token = req.body.accessToken || req.query.accessToken;
			var uid = req.body.uid || req.query.uid;
			logs("uid:" + uid + "; token:" + token);

			if (uid && token) { //If opened through FB or submitted from the page after login
				req.token = token;
				restler.get(FB_GRAPH + "/me", {query : { access_token : token }}).on('complete', function(fbUser) {
					if (fbUser.id === uid) {
						db.storeAccessToken(uid, token, function(err, user) {
							req.myShyTapUser = user;
							if (DEBUG) {
								logs("Authenticating user: " + uid + "; usr.tkn:" + (user ? user.tkn : user) + "; token: " + token);
							}
							next();
							//setGender(req, function() { next(); });
						});
					} else {
						req.authenticationError = JSON.stringify(fbUser.error) || "Authentication failed, login required.";
						if (DEBUG) {
							logs("Authentication ERROR: " + req.authenticationError + "; passed uid:" + uid + 
									"; retrieved from FB:" + fbUser.id + "; error:" + fbUser.error);
						}
						next();
					}
				});
			} else if (req.body.myUid) {
				db.getUser(req.body.myUid, function(myShyTapUser) {
					if (myShyTapUser) {
						if (token && myShyTapUser.tkn === token) {
							req.myShyTapUser = myShyTapUser;
						} else {
							req.authenticationError = "Authentication failed, login required. UserID:" + myShyTapUser.userId;
						}
					}
					if (DEBUG) {
						logs("User is supposed to be authenticated: token:" + token + "; Error:" 
								+ req.authenticationError + "; req.body.myUid:" + req.body.myUid + "; myShyTapUser:" + myShyTapUser +
								+ "; myShyTapUser.userId:" + (myShyTapUser ? myShyTapUser.userId : myShyTapUser));
					}
					next();
				});
			} else {
				req.authenticationError = "To be authorized";
				next();
			}
		}
		);

var port = process.env.PORT || 3000;

app.listen(port, function() {
	console.log("Listening on " + port);
});

app.dynamicHelpers({
	'host' : function(req, res) {
		return req.headers['host']; //'shytap.herokuapp.com';
	},
	'scheme' : function(req, res) {
		return (req.headers['x-forwarded-proto'] || 'http') + ':';
	},
	'url_no_scheme' : function(req, res) {
		return function(path) {
			return '//' + app.dynamicViewHelpers.host(req, res) + (path || '') ;
		}
	},
	'url' : function(req, res) {
		return function(path) {
			return app.dynamicViewHelpers.scheme(req, res) + app.dynamicViewHelpers.url_no_scheme(req, res)(path);
		}
	}
});

function getMainPage(req, res) {
	res.render('index.ejs', {
		layout : false,
		req : req,
		app : { id : APP_ID, name : "ShyTap"},
		google_track_id : process.env.GOOGLE_TRACK_ID
	});
}

function getUser(req, res) {
	if (req.myShyTapUser) {
		var uid = req.myShyTapUser.userId;
		getMutualTaps(uid, req.myShyTapUser.taps, function(mutualTaps) {
			res.send('{"mutualTaps":' + JSON.stringify(mutualTaps) + ', "myShyTapUser":' + JSON.stringify(req.myShyTapUser) + "}");
		});
	} else {
		res.send('{"mutualTaps":null, "myShyTapUser":null}');
	}
}

function getMutualTaps(userId, userTaps, cb) {
	db.findMutualUsers(userId, userTaps, function(mutualUsers) {
		var mutualTaps = [];
		for (var m in mutualUsers) {
			mutualTaps.push(mutualUsers[m].userId);
		}
		cb(mutualTaps);
	});
}

function handle_tap(req, res) {
	tapUnTap(req, res, db.storeTap, 'tap');
}

function handle_untap(req, res) {
	tapUnTap(req, res, db.removeTap, 'untap');
}

function tapUnTap(req, res, dbFunc, suffix) {
	var t = new Date();
	var myShyTapUser = req.myShyTapUser;
	if (myShyTapUser) {
		var myUid = myShyTapUser.userId;
		var tappedUid = req.body.userId;
		dbFunc(myUid, tappedUid, function(err, myUser) {
			if (err) {
				console.log("Error while trying to " + suffix + " the user " + tappedUid + ": " + err);
			}
			getMutualTaps(myUser.userId, myUser.taps, function(mutualTaps) {
				if (mutualTaps && mutualTaps.indexOf(tappedUid) >= 0) {
					//This can execute asynchronously:
					sendMutualTapNotification(req, myUid, tappedUid);
				}
				printTime("tapUnTap", t);
				res.send(mutualTaps ? JSON.stringify(mutualTaps) : "{error:'Error while fetching mutual taps.'}");
			});
		});
	} else {
		var error = "Unauthorized user is attempting to " + suffix + " another user.";
		console.log(error);
		printTime("tapUnTap 2", t);
		res.send("{error:'" + error + "'}");
	}
}

// Asynchronous function:
function sendMutualTapNotification(req, uid1, uid2) {
	var params = {
		client_id     : APP_ID,
		client_secret : SECRET,
		grant_type    : 'client_credentials'
	};
	restler.get(FB_GRAPH + '/oauth/access_token', { query : params }).on('complete', function(appToken) {
		var msg = "template=" + getNotifMsg(uid1) + "&" + appToken;
		restler.post(FB_GRAPH + '/' + uid2 + '/notifications', { query : msg }).on('complete', function(data) {
			logs("Notification has been sent to user " + uid2 + ": " + data);
		});
		msg = "template=" + getNotifMsg(uid2) + "&" + appToken;
		restler.post(FB_GRAPH + '/' + uid1 + '/notifications', { query : msg }).on('complete', function(data) {
			logs("Notification has been sent to user " + uid1 + ": " + data);
		});
	});
}

function getNotifMsg(uid) {
	return encodeURIComponent("@[" + uid + "] has also tapped you!");
}

//function setGender(req, cb) {
//	var user = req.myShyTapUser;
//	if (!user.g) {
//		req.facebook.me(function(me) {
//			var oppositeGender = !me.gender ? 'a' : me.gender === 'male' ? 'f' : 'm';
//			db.setGenderPreference(user.userId, user.tkn, oppositeGender, function(err, result) {
//				cb();
//			});
//		});
//	} else {
//		cb();
//	}
//}

//function handle_gender(req, res) {
//	if (!req.body.gender || ['a', 'm', 'f'].indexOf(req.body.gender) < 0) {
//		res.send("{error:'Invalid gender value.'}");
//	}
//	db.setGenderPreference(req.body.userId, req.body.accessToken, req.body.gender, function(err, result) {
//		res.send(err ? "{error:'" + err + "'}" : "{gender:" + result.g + "}");
//	});
//}

function printTime(label, startTime) {
	if (DEBUG) {
		return console.log("*** " + ((new Date().getTime() - startTime.getTime()) / 1000.0) + " sec. (" + label + ")");
	}
}

function logs(s) {
	if (DEBUG) {
		return console.log(s);
	}
}

app.get('/', getMainPage);
app.post('/', getMainPage);
app.post('/tap', handle_tap);
app.post('/untap', handle_untap);
app.get('/user', getUser);
//app.post('/gender', handle_gender);
